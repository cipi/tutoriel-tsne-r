# L'algorithme t-SNE : Illustration et pratique sous R

La réduction de dimension est un outil essentiel pour visualiser des
bases de données à grande dimension. L'ACP est un exemple de méthode
reconnue pour réduire la dimensionnalité et projeter un jeu de données
décrit par de nombreuses variables sur un espace de dimension 2 ou 3.
Cependant cette méthode de projection présente des limites dans certains
cas car elle effectue une une transformation linéaire des données.
L'algorithme **t-distributed Stochastic Neighbor Embedding**, développé
par Geoffrey Hinton et Laurens van der Maaten
([2008](#ref-maaten_visualizing_2008)), est un algorithme de machine
learning faisant parti des méthodes de réduction de dimensions
non-linéaires, très utile pour la visualisation des données à grande
dimension.

Le t-SNE permet de représenter des objets provenant d'un espace à
dimension élevée dans un espace en 2 ou 3 dimensions. Cette méthode est
capable de révéler la présence de clusters dans les données tout en
préservant les structures locales, notamment non-linéaires, de l'espace
initial. La proximité de deux points dans l'espace initial sera
similaire à celle dans l'espace final de visualisation.

Avant de plonger dans le fonctionnement de l'algorithme et ses
fondements mathématiques, nous allons simuler un exemple simpliste afin
de comprendre l'intérêt du t-SNE pour la réduction de dimensions et la
visualisation de données.

```R
library(MASS)
# On simule deux clusters de données à 3 dimensions avec des dispersions différentes : l'un concentré autour de la moyenne et l'autre plus éclaté 
set.seed(5)
Clus1 <- data.frame(mvrnorm(n=100, mu = c(0,0,0), Sigma = diag(3)))
Clus2 <- data.frame(mvrnorm(n=1500, mu = c(0,0,0), Sigma = diag(3)*8))
Clus2 <- Clus2[apply(Clus2, MARGIN = 1, function(x) norm(x, type = "2")) > 6,]

# La variable "cluster" permettra de colorer les points sur les graphiques selon leur processus de génération
df <- dplyr::bind_rows(list(Clus1, Clus2), .id = "cluster")
```

Observons les points dans l'espace initial à 3 dimensions.

```R
color1 <- "#DD8D29"
color2 <- "#46ACC8"

library(lattice)
trellis.par.set("axis.line", list(col="transparent"))

cloud(data = df, X1 ~ X2 * X3, group = cluster,
      type="p", cex = 0.6, pch = 20, col=c(color2, color1),
      xlab="", ylab="", zlab="",
      screen = list(z = -120, x = -70))
```
<div align="center">
![1](README_files/figure-markdown_strict/Visualisation des données simulées-1.png)
</div>


On veut alors réduire notre nuage initial de 3 dimensions en 2
dimensions et visualiser le résultat. Pour cela on utilise une technique
usuelle de projection linéaire qu'est l'ACP.

```R
# On utilise le package FactoMineR pour procéder à l'ACP
library(FactoMineR) 
acp.res <- PCA(df[c("X1","X2","X3")], ncp = 2, graph = FALSE, scale.unit = FALSE)

# On récupère les coordonnées des données dans l'espace en 2 dimensions pour les projeter
coord.acp <- data.frame(acp.res$ind$coord, cluster = df$cluster)

xyplot(data = coord.acp, Dim.1 ~ Dim.2, group = cluster, 
       cex = 0.7, pch = 20, col=c(color2,color1), 
       xlab="", ylab="", scales=list(draw=FALSE))
```
<div align="center">
![2](README_files/figure-markdown_strict/ACP exemple simulé-1.png)
</div>

On voit bien le problème que pose une projection linéaire lorsque la
structure initiale des données ne l'est pas. Est-ce qu'une technique de
réduction de dimension non-linéaire (aussi appelé *manifold learning*)
permettrait de retrouver la structure des données et distinguer les
points générés par des processus différents ?

```R
# Nous discutons dans une section prochaine les différents packages/implémentations du t-SNE disponibles sous R.
library(tsne) 
# Comme le t-SNE inclut une part d'aléatoire, nous fixons le seed afin de retrouver les résultats
set.seed(5)
tsne.res <- tsne(X = df[c("X1","X2","X3")], k = 2, perplexity = 30, initial_dims = 3, max_iter = 500, whiten = FALSE)

# On récupère les coordonnées des données dans l'espace en 2 dimensions pour les projeter
coord.tsne <- data.frame(tsne.res, df$cluster)

xyplot(data = coord.tsne, X1 ~ X2, group = df$cluster, 
       cex = 0.7, pch = 20, col=c(color2,color1),
       xlab="", ylab="", scales=list(draw=FALSE))
```
<div align="center">
![3](README_files/figure-markdown_strict/tSNE exemple simulé-1.png)
</div>

Le résultat de l'algorithme du t-SNE est un succès pour représenter nos
données initiales sur un graphique en 2 dimensions. En effet, il est
très facile de distinguer les deux clusters (et ce, même sans les
couleurs).

Nous allons maintenant nous pencher sur le fonctionnement de
l'algorithme.

Algorithme
----------

Comme mentionné dans l'introduction, l'algorithme du t-SNE a pour but de
trouver une configuration optimale des proximités entre les points,
c'est-à-dire que la proximité de deux points dans l'espace final (de
faible dimension) devra être similaire à la proximité observée dans
l'espace initial (de grande dimension). Ainsi les points proches (ou
éloignés) dans l'espace initial se retrouveront proches (éloignés) dans
l'espace final.

On note *x*<sub>*i*</sub> un objet de l'espace initial,
*i* = 1, ..., *N*. On note *y*<sub>*i*</sub> un objet de l'espace final.
Les distances entre les objets sont alors converties en probabilités
conditionnelles qui représentent la similarité des objets. On définit
donc une similarité conditionnelle entre deux points :

```math
p_{j|i}= \frac{\exp(-\frac{||x_{i}-x_{j}||^2}{2\sigma_{i}^2})}{\sum\_{k\neq j}\exp(-\frac{||x_{i}-x\_{k}||^2}{2\sigma_{i}^2})}
```

Avec |*x*<sub>*i*</sub> − *x*<sub>*j*</sub>| et
|*y*<sub>*i*</sub> − *y*<sub>*j*</sub>| la distance euclidienne entre
deux points. *p*<sub>*j*|*i*</sub> mesure ainsi la probabilité que
*x*<sub>*j*</sub> soit un voisin de *x*<sub>*i*</sub> selon une
distribution normale autour de *x*<sub>*i*</sub>, avec une variance
*σ*<sub>*i*</sub><sup>2</sup>.

La variance est différente pour chaque point. Sa valeur dépendra en
partie d'un (hyper)paramètre de l'algorithme nommé la *perplexité*. On
peut appréhender intuitivement la perplexité comme étant similaire au
paramètre de l'algorithme des k plus proches voisins. Avec une
perplexité faible, les variations locales auront un impact plus
important sur la représentation finale alors qu'une perplexité
importante captera mieux la structure globale des données. Il va de soi
qu'une bonne représentation finale est un équilibre entre les deux.
Ainsi, la perplexité sera probablement sujet à ajustement. Cependant,
selon Van der Maaten, les résultats du t-SNE seraient assez robustes
pour une valeur comprise entre 5 et 50. En pratique cela dépend des
situations. Afin d'en apprendre plus sur au sujet de la paramétrisation
du t-SNE et de ses écueils, nous vous renvoyons au très bon article (et
accessible) de Wattenberg, et al. ([2016](#ref-wattenberg2016how)) qui
aborde largement le sujet.

On peut maintenant définir la similarité jointe *p*<sub>*ij*</sub> :

```math
p_{ij}=\frac{p_{j|i}+p_{i|j}}{2N}
```

Similairement on peut définir une similarité jointe à partir des
distances entres les points dans l'espace final. Cependant on utilise
ici une distribution de Student à un degré de liberté.

```math
q_{ij} =\frac{\frac{1}{1 + ||y_{i} - y_{j}||^2}}{\sum\_{k\neq i}\frac{1}{1 +  ||y_{i} - y_{k}||^2}}
```

Le fait d'utiliser cette distribution à la place d'une gaussienne est un
apport de l'article de Van der Maaten et Hinton. Nous ne rentrerons pas
dans les détails ici.

Les distributions de probabilité données par *p*<sub>*ij*</sub> et
*q*<sub>*ij*</sub> permettent de construire deux matrices de
similarité : une pour l'espace initial et une pour l'espace final. Notre
but étant toujours d'avoir une représentation optimale des données
initiales dans l'espace réduit, on voudrait des matrices les plus
proches possibles.

L'algorithme va chercher les coordonnées, dans l'espace final, de chaque
point tel que la différence entre *p*<sub>*ij*</sub> et
*q*<sub>*ij*</sub> soit minimisée. On passe alors par la minimisation
de la divergence de *Kullback-Leibler* qui est une mesure de
dissimilarité entre deux distributions de probabilités. En reprenant nos
notations :

```math
KL(P||Q) = \sum_{i, j} p_{ij}~log(\frac{p_{ij}}{q_{ij}})
```

La matrice de similarité (*p*<sub>*ij*</sub>) est connue et fixée (les
coordonnées initiales sont données). La matrice de similarité
(*q*<sub>*ij*</sub>) est initialisée aléatoirement. Elle est ensuite
modifiée itérativement afin de converger vers une forme similaire à la
matrice donnée par (*p*<sub>*ij*</sub>).

Intuitivement, deux points éloignés dans l'espace réduit vont s'attirer
si ils étaient similaires dans l'espace initial. A l'inverse si ils sont
proches alors qu'ils sont dissimilaires dans l'espace initial, ils vont
se repousser. Afin de visualiser ce mécanisme, on récupère les
coordonnées des points dans l'espace final à chaque itération et on
affiche le processus itératif dans une animation :

<div align="center">
![anim](output/anim-tsne.gif)
</div>

Il est important de noter que l'optimisation est non convexe, et donc
que plusieurs minimas locaux peuvent être atteints. Ainsi, à chaque
exécution de l'algorithme, on obtiendra un résultat différent.

Implémentations et packages R
-----------------------------

Sous R, deux packages fournissant une implémentation du t-SNE existent.
Très utile, le site de Laurens van der Maaten référence toutes les
implémentations disponibles (sous Python, R, Matlab, ...) mais fournit
également une FAQ intéressante quant au fonctionnement du t-SNE.

Le package [tsne](https://CRAN.R-project.org/package=tsne) est la
première implémentation disponible sous R. Elle reprend l'aglorithme
comme décrit dans le papier original et est codé en "pur R".

Le package [Rtsne](https://CRAN.R-project.org/package=Rtsne) est un
package proposant un wrapper du code C++ d'une implémentation utilisant
l'algorithme de Barnes-Hutt. Sans rentrer dans les détails, cette
modification ([2014](#ref-maaten_accelerating_2014)) permet d'accélérer
considérablement les performances du t-SNE, et rend ainsi possible son
utilisation pour les très larges bases de données (millions d'objets).
Voici une petite description de cette fonction et de ses principaux
arguments :

```R
library(Rtsne)
Rtsne(X, initial_dims = 50, pca = TRUE, dims = 2, perplexity = 30, theta = 0.5, max_iter = 1000, verbose = FALSE, ...)
```

-   <span style="color:darkred">X</span> une matrice ou un dataframe ; à
    noter qu'une matrice de distance peut aussi être donnée
-   <span style="color:darkred">dims</span> : le nombre de dimensions de
    l'espace final (en général 2 ou 3)
-   <span style="color:darkred">perplexity</span> : le paramètre de
    perplexité
-   <span style="color:darkred">theta</span> : un paramètre d'arbitrage
    (ajouté dans l'implémentation Barnes-Hutt) entre la vitesse et la
    précision ; pour retrouver l'implémentation originale, fixer le
    paramètre à 0.0
-   <span style="color:darkred">max\_iter</span> : le nombre
    d'itérations de l'algorithme
-   <span style="color:darkred">verbose</span> : une simple option pour
    afficher le progrès de l'algorithme dans la console
-   <span style="color:darkred">initial\_dims</span> et <span
    style="color:darkred">pca</span> sont des paramètres qui permettent
    une réduction initiale de la dimension (de l'espace original) par
    une ACP
-   d'autres arguments sont disponibles et permettent notamment de
    modifier des paramètres jouant sur la phase d'optimisation du t-SNE

En règle général, il est plus intéressant d'utiliser le package Rtsne à
la fois pour sa rapidité et sa flexibilité.

Exemple sur données réelles et comparaison avec l'ACP
-----------------------------------------------------

A venir

Références
----------

Maaten, Laurens van der. 2014. “Accelerating T-SNE Using Tree-Based
Algorithms.” *Journal of Machine Learning Research*.
<http://jmlr.org/papers/v15/vandermaaten14a.html>.

Maaten, Laurens van der, and Geoffrey Hinton. 2008. “Visualizing Data
Using T-SNE.” *Journal of Machine Learning Research*.
<http://www.jmlr.org/papers/v9/vandermaaten08a.html>.

Wattenberg, Martin, Fernanda Viégas, and Ian Johnson. 2016. “How to Use
T-Sne Effectively.” *Distill*. <http://distill.pub/2016/misread-tsne>.

